﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestTask;

namespace UnitTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void FindStrTest1()
        {
            string str = "2017-05-23 12:24:03.2322|Info||Handle|Request for 37041_713_1_GetImages";
            string expected = "2017-05-23, 12:24:03.2322, 37041_713_1_GetImages";

            string result = Program.FindMyString(str);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void FindStrTest2()
        {
            string str = "2017-05-23 12:24:03.2322||||Request for 37041_713_1_GetImages";
            string expected = "2017-05-23, 12:24:03.2322, 37041_713_1_GetImages";

            string result = Program.FindMyString(str);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void FindStrTest3()
        {
            string str = "2017-05-23 12:24:03.2322|Info||Handle|Request for 37041_713_GetImages";
            string expected = null;

            string result = Program.FindMyString(str);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void FindStrTest4()
        {
            string str = "2017-05-23 12:24|Info||Handle|Request for 37041_713_1_GetImages";
            string expected = null;

            string result = Program.FindMyString(str);

            Assert.AreEqual(expected, result);
        }
    }
}
