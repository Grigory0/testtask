﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace TestTask
{
    public class Program
    {
        public static string FindMyString (string str)
        {
            string pattern = @"\b(\d{4}-\d{2}-\d{2})\s(\d{2}:\d{2}:\d{2}\.\d{4}).+?Request\sfor\s(\d+_\d+_\d+_\w+)\b";
            Regex regex = new Regex(pattern);
            Match match = regex.Match(str);
            if (match.Success)
            {  
                return match.Groups[1].Value + ", " + match.Groups[2].Value + ", " + match.Groups[3].Value;
            } else
            {
                return null;
            }
        }

        static void Main(string[] args)
        {
            string fileName = "../../EventService.log";
            string fileNameWrite = "../../Result.log";
            StreamReader reader = null;
            try
            {
                reader = new StreamReader(fileName);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.ReadLine();
                return;
            }
            StreamWriter writer = new StreamWriter(fileNameWrite);
            while (!reader.EndOfStream) {
                string str = reader.ReadLine();
                string strMy = FindMyString(str);
                if (!(strMy==null)) {
                    writer.WriteLine(strMy);
                }
            }
            reader.Close();
            writer.Close();
            Console.WriteLine("File processed successfully");
            Console.ReadLine();
        }
    }
}
